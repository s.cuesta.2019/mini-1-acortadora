import socket

class WebApp:
    def __init__(self, hostname, port):
        self.hostname = hostname
        self.port = port

    def parse(self, request):
        return request.decode('utf-8').split(' ', 2)[:2]

    def process(self, parsed_request):
        return "200 OK", "<html><body><h1>webApp funciona</h1></body></html>"

    def run(self):
        servidor = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        servidor.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        servidor.bind((self.hostname, self.port))
        servidor.listen(5)
        print(f'Sirviendo en {self.hostname}:{self.port}')

        while True:
            print('Esperando conexiones...')
            conexion, direccion_cliente = servidor.accept()
            print(f'El cliente con dirección {direccion_cliente} se ha conectado')

            request = conexion.recv(2048)
            parsed_request = self.parse(request)
            code, msg = self.process(parsed_request)
            response = f'HTTP/1.1 {code}\r\n\r\n{msg}\r\n'
            conexion.send(response.encode('utf-8'))
            conexion.close()

if __name__ == "__main__":
    web_app = WebApp('localhost', 1234)
    web_app.run()
